############################################################################
#                                                                          #
#               ------- Company Apposing Aliases --------                  #
#                                                                          #
############################################################################


function toot_server-fn {
    ssh -t root@178.62.105.250 'cd /var/www/toot-backend; 
    php artisan; 
    cat /etc/issue; 
    git --version;
    mysql --version;
    service mysql status;
    composer --version;
    php --version;
    php artisan --version;
    nginx -v;
    service nginx status;
    supervisord -v;
    redis-server -v;
    php artisan horizon:status;
    php artisan horizon:list;
    php artisan horizon:supervisors;
    git branch;  bash -l'   
}

function fanoty_server-fn {
    ssh -t root@46.101.33.118 'cd /var/www/cms;
    php artisan;
    cat /etc/issue;
    git --version;
    mysql --version;
    composer --version;
    php --version;
    php artisan --version;
    apache2 -v;
    git branch; bash -l'   
}

function huktup_server-fn {
    ssh -t root@138.68.129.49 'cd /var/www/huktup/laradock; docker-compose ps;'
}

alias app="cd ~/projects/apposing"
alias toot="cd ~/projects/apposing/toot/laradock"
alias huk="cd ~/projects/apposing/huktup/laradock"
alias fanoty="cd ~/projects/apposing/fanoty/laradock"
alias toot_server=toot_server-fn
alias fanoty_server=fanoty_server-fn
alias huk_server=huktup_server-fn
alias huktup_server=huktup_server-fn