############################################################################
#                                                                          #
#               ------- Company Full Comms Aliases --------                #
#                                                                          #
############################################################################

# =================== Southern

function southern_staging-fn {
    ssh -t forge@18.132.80.111 'cd /home/forge/default;
    php artisan;
    php artisan dusk:chrome-driver --detect;
    cat /etc/issue;
    git --version;
    mysql --version;
    composer --version;
    php --version;
    php artisan --version;
    nginx -v;
    service nginx status;
    redis-server -v;
    bash -l'
}

alias south="cd ~/projects/fullcomms/Southern/laradock"
alias southen=south
alias south.wiki="cd ~/projects/fullcomms/Southern.wiki"
alias southern_staging=southern_staging-fn
alias south_staging=southern_staging-fn
alias southStaging=southern_staging-fn

# ---- Lift containers WSL
alias dcsouth= "cd ~/projects/fullcomms/Southern/laradock && dcu nginx mysql redis selenium"
alias dcsouthw=dcsouth
alias southup=dcsouth

# ---- Lift containers MAC
alias dcsouthm="cd ~/projects/fullcomms/Southern/laradock && dcu nginx mysql redis"
alias dcsouthmac=dcsouthm
alias southupm=dcsouthm

# ---- Put down containers
alias southdown="cd ~/projects/fullcomms/Southern/laradock && dcd"
alias southd=southdown


# =================== The 4th Utility

alias the4th="cd ~/projects/fullcomms/The4thUtilityWebsite/laradock"
alias 4th=the4th

# ---- Lift containers WSL
alias dc4th= "cd ~/projects/fullcomms/The4thUtilityWebsite/laradock && dcu nginx mysql"
alias 4thup=dc4th
alias 4thdown="cd ~/projects/fullcomms/The4thUtilityWebsite/laradock && dcd"


# =================== CompleteMoves

alias completeMoves="cd ~/projects/fullcomms/CompleteMoves/laradock"
alias complete=completeMoves
alias cm=completeMoves

# ---- Lift containers WSL
alias completeup= "cd ~/projects/fullcomms/CompleteMoves/laradock && dcu nginx mysql"