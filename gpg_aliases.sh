############################################################################
#                                                                          #
#                 ------- Useful GPG Aliases --------                      #
#                                                                          #
############################################################################

# Export Public Key
function export_GPG-fn {
	  gpg --armor --export $1
}

alias reload_gpg='eval $(gpg-agent --daemon)'
alias reset_agent='gpgconf --kill gpg-agent'
alias reload_agent='gpgconf --launch gpg-agent'

alias gpg_list='gpg --list-secret-key --keyid-form LONG'
alias gpg_export=export_GPG-fn
# export GPG_TTY=$(tty)
