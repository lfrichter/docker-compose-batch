############################################################################
#                                                                          #
#          ------- Useful Mac ohMyZsh Aliases --------                     #
#                                                                          #
############################################################################


alias zshconfig="nano ~/.zshrc"
alias ohmyzsh="nano ~/.oh-my-zsh"
alias edit_batch='open -e ~/.zshrc'

