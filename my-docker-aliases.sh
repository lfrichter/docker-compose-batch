############################################################################
#                                                                          #
#               ------- Useful My Docker Aliases --------                     #
#                                                                          #
#     # Installation :                                                     #
#     copy/paste these lines into your .bashrc or .zshrc file or just      #
#     type the following in your current shell to try it out:              #
#     wget -O - https://bitbucket.org/lfrichter/docker-compose-batch/src/master/my-docker-aliases.sh | bash
#                                                                          #
#     # Usage:                                                             #
#     daws <svc> <cmd> <opts> : aws cli in docker with <svc> <cmd> <opts>  #
#     dc             : docker-compose                                      #
#     dcu            : docker-compose up -d                                #
#     dcd            : docker-compose down                                 #
#     dcr            : docker-compose run                                  #
#     dex <container>: execute a bash shell inside the RUNNING <container> #
#     di <container> : docker inspect <container>                          #
#     dim            : docker images                                       #
#     dip            : IP addresses of all running containers              #
#     dl <container> : docker logs -f <container>                          #
#     dnames         : names of all running containers                     #
#     dps            : docker ps                                           #
#     dpsa           : docker ps -a                                        #
#     drmc           : remove all exited containers                        #
#     drmid          : remove all dangling images                          #
#     drun <image>   : execute a bash shell in NEW container from <image>  #
#     dsr <container>: stop then remove <container>                        #
#                                                                          #
############################################################################

function dnames-fn {
	for ID in `docker ps | awk '{print $1}' | grep -v 'CONTAINER'`
	do
    	docker inspect $ID | grep Name | head -1 | awk '{print $2}' | sed 's/,//g' | sed 's%/%%g' | sed 's/"//g'
	done
}

function dip-fn {
    echo "IP addresses of all named running containers"

    for DOC in `dnames-fn`
    do
        IP=`docker inspect $DOC | grep -m3 IPAddress | cut -d '"' -f 4 | tr -d "\n"`
        OUT+=$DOC'\t'$IP'\n'
    done
    echo $OUT|column -t
}

function dex-fn {
	docker exec -it $1 ${2:-bash}
}

function di-fn {
	docker inspect $1
}

function dl-fn {
	docker logs -f $1
}

function drun-fn {
	docker run -it $1 $2
}

function dcr-fn {
	docker-compose run $@
}

function dsr-fn {
	docker stop $1;docker rm $1
}

function drmc-fn {
       docker rm $(docker ps --all -q -f status=exited)
}

function drmid-fn {
       imgs=$(docker images -q -f dangling=true)
       [ ! -z "$imgs" ] && docker rmi "$imgs" || echo "no dangling images."
}

# in order to do things like dex $(dlab label) sh
function dlab {
       docker ps --filter="label=$1" --format="{{.ID}}"
}

function dc-fn {
        docker-compose $*
}

function dcu-fn {
       docker-compose up -d $*
}

function dcup-fn {
       docker-compose up -d nginx mysql $*
}

function dceb-fn {
       docker-compose exec -u laradock $* bash
}

function dcebr-fn {
       docker-compose exec $* bash
}

function dcrebuild-fn {
       docker-compose build --no-cache $*
}

function laravelnew-fn {
       composer create-project --prefer-dist laravel/laravel $*
       cd $*
}


function d-aws-cli-fn {
    docker run \
           -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
           -e AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION \
           -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
           amazon/aws-cli:latest $1 $2 $3
}

function get_laradock-fn {
       git clone https://github.com/Laradock/laradock.git
       cd laradock
       cp .env.example .env
       cp ./mysql/docker-entrypoint-initdb.d/createdb.sql.example ./mysql/docker-entrypoint-initdb.d/createdb.sql
       nano ./mysql/docker-entrypoint-initdb.d/createdb.sql
       nano .env
}

function scp_local_to_huk-fn {
       scp -p $1 root@138.68.129.49:$2
}

# ~/.bashrc
alias daws=d-aws-cli-fn
alias dc=dc-fn
alias dcu=dcu-fn
alias dcup=dcup-fn
alias dceb=dceb-fn
alias dcebr=dcebr-fn
alias dcrebuild=dcrebuild-fn
alias laravelnew=laravelnew-fn
alias laravel_new=laravelnew-fn
alias scp_local_to_huk=scp_local_to_huk-fn
alias dcrebuild_default="docker-compose build --no-cache workspace php-fpm"
alias dcrebuild_php="docker-compose build --no-cache php-fpm"
alias dcrebuild_workspace="docker-compose build --no-cache workspace"
alias dcew="docker-compose exec -u laradock workspace bash"
alias dcewr="docker-compose exec workspace bash"
alias dcd="docker-compose down"
alias dcr=dcr-fn
alias dex=dex-fn
alias di=di-fn
alias dim="docker images"
alias dip=dip-fn
alias dl=dl-fn
alias dnames=dnames-fn
alias dps="docker ps"
alias dpsa="docker ps -a"
alias drmc=drmc-fn
alias drmid=drmid-fn
alias drun=drun-fn
alias dsp="docker system prune --all"
alias dsr=dsr-fn
alias get_laradock=get_laradock-fn
alias getlaradock=get_laradock-fn

# Batch file edit
alias batch_edit="nano ~/.bashrc"
alias edit_batch="nano ~/.bashrc"
alias batch_upd=" . ~/.bashrc"
alias batch_update=" . ~/.bashrc"







